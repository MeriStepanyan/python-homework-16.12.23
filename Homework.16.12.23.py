foo=lambda:(lambda :(lambda y:y))
func=foo()
print(func()(3))


# 1. Write a program that takes three words as input from the user and prints them on the same line 
#with a '-' as the separator.
def  words(str1, str2, str3):
    print(str1,str2,str3, sep="-")
str1="Hello"
str2="World"
str3="!"
words(str1, str2, str3)

# 2. Write a program that takes three numbers as input and prints their sum, 
#separating each number with a '+' sign. Use the 'sep' argument for formatting.

num1=10
num2=13
num3=14
sum=num1+num2+num3
print(num1,num2,num3, sep="+", end=" ")
print("=",sum)

# 3. Create a script that writes a list of numbers (1 to 5) to a file named "numbers.txt" using the 'print()' 
#function and the 'file' argument.

x=[x for x in range(1,5)]
file1=open('numbers.txt','w')
print(x,file=file1)

# 4. Create a program that takes user input for three lines of text and writes them to a 
#file named "output.txt". Each line should end with a custom ending, such as " -- End of Line."
string= "GO placidly amid the noise and haste, and remember what peace there may be in silence.\nAs far as possible without surrender be on good terms with all persons.\nSpeak your truth quietly and clearly; and listen to others, even the dull and ignorant; they too have their story."
f=open('input.txt',"w")
print(string, file=f)  
f.close()
f=open('input.txt')  
f1=open('output.txt','w')
for line in f:
    line = line.rstrip('\n')
    line+=" -- End of line---"
    print(line, file=f1)
f.close()
f1.close()


str1="GO placidly amid the noise and haste"
str2="and remember what peace there may be in silence."
str3="As far as possible without surrender be on good terms with all persons."
file2=open("output_.txt","a")
print(str1+"-End of line-", file=file2)
print(str2+"-End of line-", file=file2)
print(str3+"-End of line-", file=file2)

# 5. Write a recursive function to print all the numbers from 1 to a given positive integer n in
# increasing order.
def countdown(n):
    if n == 0:
        return 0
    
    countdown(n-1)
    print(n)
    
countdown(7)  

# 6. Implement a recursive function to print all the numbers from a 
# given positive integer n to 1 in decreasing order.

"""def print2(x):
    while(x):
        print(x[-1])
        x=x[:-1]
x=[x for x in range(1,7)]        
print2(x)"""

def countdown(n):
    if n == 0:
        return 0
    print(n)
    countdown(n-1)
    
    
countdown(7)   
        

# 7. Write a recursive function to calculate the factorial of a given positive integer n.

def factorial(n):
    if n==0 or n==1:
        return 1
    return n*factorial(n-1)
print(factorial(5)) 

# 8. Write a recursive function to find the sum of the digits of a positive integer.

def sum(n):
    if n==0:
        return 0
    
    return n%10+sum(n//10)
print(sum(151)) 